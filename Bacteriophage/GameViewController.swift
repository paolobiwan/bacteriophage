//
//  GameViewController.swift
//  prove menu
//
//  Created by Antonio Giugliano on 28/04/2020.
//  Copyright © 2020 Antonio Giugliano. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let scene1 = MenuScene(size: view.bounds.size)
        let skView = view as! SKView
        skView.showsFPS = true
        skView.showsNodeCount = true
//        skView.showsPhysics = true
        skView.ignoresSiblingOrder = true
        scene1.scaleMode = .resizeFill
        skView.presentScene(scene1)
//        UIView.appearance().isExclusiveTouch = true
        
        
    }

    override var shouldAutorotate: Bool {
        return false
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
