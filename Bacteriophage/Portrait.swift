//
//  portrait.swift
//  Joystick
//
//  Created by Girolamo Pinto on 02/05/2020.
//  Copyright © 2020 Girolamo Pinto. All rights reserved.
//

import SpriteKit

class Portrait: SKNode {
    
    let background : SKSpriteNode
    let size: CGSize
    
    init(size: CGSize) {
        self.size = size
        
        background = SKSpriteNode(color: UIColor(patternImage: UIImage(named: "background")!), size: size)
        
        super.init()
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(){
        addChild(background)
        background.anchorPoint = CGPoint(x: 0, y: 0)
    }
}
