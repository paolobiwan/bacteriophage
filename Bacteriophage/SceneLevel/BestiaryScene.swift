//
//  BestiaryScene.swift
//  Bacteriophage
//
//  Created by Antonio Giugliano on 10/05/2020.
//  Copyright © 2020 InDaCave Studios. All rights reserved.
//

import Foundation
import SpriteKit

class BestiaryScene : SKScene{
     var background = SKSpriteNode(imageNamed: "kurtz")
        var backButton = SKSpriteNode(imageNamed: "Back")
        var logo = SKSpriteNode(imageNamed: "Bestiary")
        override func didMove(to view: SKView) {
            //SETTAGGIO BACKGROUND
            background.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
            background.zPosition = -1
            background.setScale(1.15)
            //SETTAGGIO LOGO
            logo.size = CGSize(width: 293, height: 68)
            logo.setScale(0.7)
            logo.position = CGPoint(x: frame.midX, y: 0.85*frame.maxY)
            //SETTAGGIO PULSANTE BACK
            backButton.position = CGPoint(x: frame.midX, y: 0.10*frame.maxY)
            backButton.size = CGSize(width: 210, height: 70)
            backButton.setScale(0.5)
            
            
            
            
            
            
            addChild(logo)
            addChild(background)
            addChild(backButton)
        }//fine didMove
        
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            if let touch = touches.first {
                let pos = touch.location(in: self)
                let node = self.atPoint(pos)
                if node == backButton {
                    if view != nil {
                        let transition:SKTransition = SKTransition.moveIn(with: .right, duration: 0.5)
                        let scene:SKScene = MenuScene(size: self.size)
                        self.view?.presentScene(scene, transition: transition)
                    }
                }// RITORNO ALLA MENU SCENE
            }//fine if let touch
            
    }// fine touchesBegan
        
}
