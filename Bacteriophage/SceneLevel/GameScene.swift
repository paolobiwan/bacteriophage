//
//  GameScene.swift
//  prove menu
//
//  Created by Antonio Giugliano on 28/04/2020.
//  Copyright © 2020 Antonio Giugliano. All rights reserved.
//

import SpriteKit
import GameplayKit


class GameScene: SKScene {
    var pauseButton = SKSpriteNode()
    var speakerOnButton = SKSpriteNode()
    var speakerOffButton = SKSpriteNode()
    var enemy = SKSpriteNode()
    let pausedLabel = SKSpriteNode()
    let pauseLogo = SKSpriteNode(imageNamed: "Pause-1")
    let exitButton = SKSpriteNode(imageNamed: "Main menu B")
    let optionButton = SKSpriteNode(imageNamed: "Options B")
    let resumeButton = SKSpriteNode(imageNamed: "Resume B")
    var attackButton = SKSpriteNode()
    let gameLayer = SKNode() // il layer del gioco
    let pauseLayer = SKNode() //il layer della pausa
    let settingLayer = SKNode() //il layer dei pulsanti nel game (pausa, volume etc...)
    let velocityMultiplier: CGFloat = 0.12
    var isCharacterMoving = false
    var viewBounds: CGSize?
    let enemyRadius : CGFloat = 150 // anche questa
    var attackButtonPressed = false // per il momento anche questa
    var startingXHeroPosition : CGFloat = 0.0
    var startingYHeroPosition : CGFloat = 0.0
   
   
    var backgrounds = [Portrait]()
    
    lazy var background: SKSpriteNode = {
        //        questo è il background, centrato in zero e scalato con la dimensione dello schermo
        var sprite = SKSpriteNode(imageNamed: "background")
        sprite.position = CGPoint(x: self.frame.size.width/2, y: self.frame.size.height/2)
        sprite.zPosition = 0
        sprite.scale(to: CGSize(width: self.frame.size.width, height: self.frame.size.height))
        return sprite
    }()
    
    
    lazy var hero: SKSpriteNode = {
       
        var sprite = SKSpriteNode(imageNamed: "hero")
        sprite.zPosition = 4 //giusto per non farlo buggare
        sprite.scale(to: CGSize(width: self.frame.size.width/5, height: self.frame.size.height/9))
        return sprite
    }()
    
    lazy var analogJoystick: TLAnalogJoystick = {
        let js = TLAnalogJoystick(withDiameter: 100)
        js.base.image = UIImage(named: "substrate")
        js.handle.image = UIImage(named: "joystick")
        js.zPosition = 3
        return js
    }()
    
    lazy var cameraNode : SKCameraNode = {
        let node = SKCameraNode()
        camera = node
        node.position.x = self.frame.size.width / 2
        node.position.y = self.frame.size.height / 2
        return node
    }()
    
    lazy var moveJoystickHiddenArea : TLAnalogJoystickHiddenArea = {
        let area = TLAnalogJoystickHiddenArea(rect: CGRect(x: 0.0, y: 0.0, width: self.frame.width, height: 10000))
        area.strokeColor = UIColor.gray.withAlphaComponent(0)
        return area
    }()
    
    override init(size: CGSize){
           super.init(size: size)
           viewBounds = size
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMove(to view: SKView) {
        
//        setupPlayableArea() // crea la playable area
        createBUTTOMS() //crea i vari bottoni
        setupBackground() //setta il background
        setupHero() //setta l'eroe
        setupEnemies() //setta il nemico
        setupCameraNode() //setta la camera
        manageJoystick() //setta il joystick
        
        

              
        
        
        
        
        addChild(gameLayer)
        
        
    }// fine didMove
    
    override func update(_ currentTime: TimeInterval) {
        if hero.position.y > startingYHeroPosition{
            cameraNode.position.y = hero.position.y
        }
    }// fine update
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let pos = touch.location(in: self)
            let node = self.atPoint(pos)
            
            if node == pauseButton {
                
                if view != nil {
                    pauseButtonF()
                    
                }
            }else if node == resumeButton{
                unpauseButtonF()
                
            }
            else if node == exitButton{
                if view != nil {
                    let transition:SKTransition = SKTransition.fade(withDuration: 1)
                    let scene:SKScene = MenuScene(size: self.size)
                    self.view?.presentScene(scene, transition: transition)
                }
            }
            else if node == speakerOffButton{
                muteF()
            }
            else if node ==  speakerOnButton{
                smuteF()
            }
            else if node == optionButton{
                if view != nil {
                    let transition:SKTransition = SKTransition.fade(withDuration: 1)
                    let scene:SKScene = OptionScene(size: self.size)
                    self.view?.presentScene(scene, transition: transition)
                }
            }
        }//fin if let touch
    }//fine touchbegan
    
   
    
    func muteF(){
        // algortitmo per mutare il gioco
        speakerOffButton.removeFromParent()
        settingLayer.addChild(speakerOnButton)
        
    }
    
    func smuteF(){
        //algoritmo per smutare il gioco
        speakerOnButton.removeFromParent()
        settingLayer.addChild(speakerOffButton)
    }
    
    func pauseButtonF()
    {//FUNZIONE CHE METTE IL GIOCO IN PAUSA
        pauseButton.removeFromParent()
        gameLayer.isPaused = true
        
        cameraNode.addChild(pauseLayer)
    }
    
    func unpauseButtonF()
    {//FUNZIONE CHE RIPRENDE IL GIOCO DALLA PAUSA
        pauseLayer.removeFromParent()
        gameLayer.isPaused = false
        settingLayer.addChild(pauseButton)
    }
    
    func createBUTTOMS() {
        //QUESTA FUNZIONE CREA TUTTI I BOTTONI PER ACCEDERE AI VARI MENU
        
        //        PULSANTE PER IL BOTTONE DELL'ATTACCO
        attackButton.zPosition = 7
        attackButton.position = CGPoint(x: (size.width)*0.35, y: -size.height*0.40)
        attackButton.texture = SKTexture(imageNamed: "attackButton")
        attackButton.size = CGSize(width: 100, height: 100)
        cameraNode.addChild(attackButton)
        //        PULSANTE PER LA PAUSA NEL GIOCO
        pauseButton.size = CGSize(width: 30, height: 30)
        pauseButton.position = CGPoint(x: (size.width)*0.40, y: size.height*0.45)
        pauseButton.texture = SKTexture(imageNamed: "pause")
        pauseButton.zPosition = 10
        settingLayer.addChild(pauseButton)
        //        PULSANTE PER MUTARE IL GIOCO
        speakerOnButton.zPosition = 10
        speakerOnButton.size = CGSize(width: 30, height: 30)
        speakerOnButton.position = CGPoint(x: -(size.width)*0.40, y: size.height*0.45)
        speakerOnButton.texture = SKTexture(imageNamed: "speaker-off")
        settingLayer.addChild(speakerOnButton)
        //        PULSANTE PER SMUTARE IL GIOCO
        speakerOffButton.zPosition = 10
        speakerOffButton.size = CGSize(width: 30, height: 30)
        speakerOffButton.position = CGPoint(x: -(size.width)*0.40, y: size.height*0.45)
        speakerOffButton.texture = SKTexture(imageNamed: "speaker-on")
        
        //        LAYER DELLA PAUSA
        pausedLabel.position = CGPoint(x: pauseLayer.position.x, y: pauseLayer.position.y)
        pausedLabel.size = CGSize(width: frame.width, height: frame.height)
        pausedLabel.color = .black
        pausedLabel.alpha = 0.5
        pausedLabel.zPosition = 9
        pauseLayer.addChild(pausedLabel)
        // LOGO DELLA PAUSA
        pauseLogo.size = CGSize(width: 197, height: 49)
        pauseLogo.zPosition = 10
        pauseLogo.position = CGPoint(x: pauseLayer.position.x, y: pauseLayer.position.y+350)
        pauseLayer.addChild(pauseLogo)
        //        PULSANTE PER L'USCITA DAL GIOCO
        exitButton.position = CGPoint(x: pauseLayer.position.x, y: pauseLayer.position.y-200)
        exitButton.size = CGSize(width: 305, height: 58)
        exitButton.color = .brown
        exitButton.zPosition=10
        pauseLayer.addChild(exitButton)
        
        // PULSANTE PER LE OPZIONI
        optionButton.zPosition = 12
        optionButton.color = .purple
        optionButton.size = CGSize(width: 305, height: 58)
        optionButton.position = CGPoint(x: pauseLayer.position.x, y: exitButton.position.y+150)
        pauseLayer.addChild(optionButton)
        
        //        PULSANTE PER IL RESUME
        resumeButton.position = CGPoint(x: pauseLayer.position.x, y: optionButton.position.y+150)
        resumeButton.size = CGSize(width: 305, height: 58)
        resumeButton.color = .brown
        resumeButton.zPosition=11
        pauseLayer.addChild(resumeButton)
        
        
    }//fine createBUTTOMS
    
    func setupPlayableArea(){
        let playableArea = SKPhysicsBody(edgeLoopFrom: CGRect(x: 0.0, y: 0.0, width: self.frame.width, height: 10000))
        self.physicsBody = playableArea
    }
    
    func setupHero(){
        //mi carica l'eroe, capire bene l'anchor point
        startingXHeroPosition = self.frame.size.width / 2
        startingYHeroPosition = self.frame.size.height / 2
        hero.position = CGPoint(x: startingXHeroPosition, y: startingYHeroPosition)
        hero.physicsBody = SKPhysicsBody(texture: hero.texture!, size: hero.size)
        hero.physicsBody?.isDynamic = true
        hero.physicsBody?.affectedByGravity = false
        hero.physicsBody?.categoryBitMask = PhysicsCategory.Hero
        hero.physicsBody?.collisionBitMask = PhysicsCategory.Enemy
        gameLayer.addChild(hero)
    }//fine setupHero
    
    func setupLifeHero(){
        let heroHpBackground = SKShapeNode(rectOf: CGSize(width: 100, height: 10), cornerRadius: 5)
        heroHpBackground.position = CGPoint(x: self.frame.size.width/2, y: self.frame.size.height/2)
        heroHpBackground.zPosition = 100
        heroHpBackground.strokeColor = .black
        gameLayer.addChild(heroHpBackground)
        
        let heroHpContainer = SKShapeNode(rectOf: CGSize(width: 150, height: 10), cornerRadius: 5)
        heroHpContainer.position = CGPoint(x: self.frame.size.width/2, y: self.frame.size.height/2)
        heroHpContainer.zPosition = 100
        heroHpContainer.strokeColor = .black
        gameLayer.addChild(heroHpContainer)
    }//fine setup lifeHero
    
    func setupEnemies(){
        let enemyTexture = SKTexture(imageNamed: "Vibrio")
        enemy = SKSpriteNode(texture: enemyTexture)
        enemy.setScale(0.25)
        enemy.position = CGPoint(x: self.frame.size.width*0.7, y: self.frame.size.height*0.9)
        enemy.zPosition = 4
        enemy.name = "Rococcus"
        self.physicsBody = SKPhysicsBody(texture: enemyTexture, size: enemyTexture.size())
        enemy.physicsBody?.isDynamic = true
        enemy.physicsBody?.affectedByGravity = false
        enemy.physicsBody?.categoryBitMask = PhysicsCategory.Enemy
        enemy.physicsBody?.collisionBitMask = PhysicsCategory.Hero
        gameLayer.addChild(enemy)
        
    }// fine setup enemy
    
    func setupBackground(){
        //mi carica il background, ho cambiato x ed y da 0.5 a 0
        gameLayer.addChild(background)
    }//fine setupBackground
    
    func setupCameraNode(){
        gameLayer.addChild(cameraNode)
        cameraNode.addChild(settingLayer)
        
    }//fine setupCameraNode
    
    func manageJoystick(){
        moveJoystickHiddenArea.joystick = analogJoystick
        analogJoystick.isMoveable = true
        
        gameLayer.addChild(moveJoystickHiddenArea)
        
        joystickActionSettings()
    }//fine manageJoystick
    
    // azioni del joystick
       private func joystickActionSettings(){
           analogJoystick.on(.begin) { (joystick) in
               self.isCharacterMoving = true
           }
           analogJoystick.on(.move) { [unowned self] joystick in

               if self.isCharacterMoving == true{
                //AGGIORNAMENTO POSIZIONE DEL PERSONAGGIO
                   self.hero.position = CGPoint(x: self.hero.position.x + (joystick.velocity.x * self.velocityMultiplier), y: self.hero.position.y + (joystick.velocity.y * self.velocityMultiplier))
                       self.hero.zRotation = joystick.angular
                
               }//fine dell'if
           }
           analogJoystick.on(.end) { (joystick) in
               self.hero.removeAllActions()
           }
       }//fine joystickActionSettings
    
    
    
}//fine GameScene
