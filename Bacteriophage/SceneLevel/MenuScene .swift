//
//  MenuScene .swift
//  prove menu
//
//  Created by Antonio Giugliano on 28/04/2020.
//  Copyright © 2020 Antonio Giugliano. All rights reserved.
//

import Foundation
import SpriteKit


class MenuScene: SKScene{
    
    var logo = MenuButtons(size: CGSize(width: 293, height: 68), texture: SKTexture(imageNamed: "Logo"), color: .blue, zPosition: 10)
    
    var icona = MenuButtons(size: CGSize(width: 81, height: 115), texture: SKTexture(imageNamed: "Icona"), color: .yellow, zPosition: 10)
    
    var background = SKSpriteNode(imageNamed: "kurtz")
    
    var continueButton = MenuButtons(size: CGSize(width: 305, height: 58), texture: SKTexture(imageNamed: "Continue B"), color: .red, zPosition: 10)
    
    var newGameButton = MenuButtons(size: CGSize(width: 305, height: 58), texture: SKTexture(imageNamed: "New Game B"), color: .black, zPosition: 10)
    
    var optionButton = MenuButtons(size: CGSize(width: 305, height: 58), texture: SKTexture(imageNamed: "Options B"), color: .purple, zPosition: 10)
    
    var bestiaryButton = MenuButtons(size: CGSize(width: 305, height: 58), texture: SKTexture(imageNamed: "Bestiary B"), color: .white, zPosition: 10)
    
    var creditsButton = MenuButtons(size: CGSize(width: 305, height: 58), texture: SKTexture(imageNamed: "Credits B"), color: .cyan, zPosition: 10)
   
    
    

    
    override func didMove(to view: SKView) {
        
        backgroundColor = .gray
        
        //SETTAGGIO BACKGROUND
        background.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
        background.zPosition = -1
        background.setScale(1.15)
        
        logo.position = CGPoint(x: frame.midX, y: 0.85*frame.maxY)
        
        //SETTAGGIO ICONA
        icona.position = CGPoint(x: frame.midX, y: 0.70*frame.maxY)
        
        
        //SETTAGGIO PULSANTE CONTINUA GIOCO
       
        continueButton.position = CGPoint(x: frame.midX, y: 0.55*frame.maxY)
        
        //SETTAGGIO PULSANTE NUOVO GIOCO
 
        newGameButton.position = CGPoint(x: frame.midX, y: 0.45*frame.maxY)
        
        //SETTAGGIO PULSANTE OPZIONI
        
        optionButton.position = CGPoint(x: frame.midX, y: 0.35*frame.maxY)
        
        //SETTAGGIO PULSANTE BESTIARIO
        
        bestiaryButton.position = CGPoint(x: frame.midX, y: 0.25*frame.maxY)
        
        //SETTAGGIO PULSANTE CREDITS
      
        creditsButton.position = CGPoint(x: frame.midX, y: 0.15*frame.maxY)
        
        self.addChild(logo)
        self.addChild(icona)
        self.addChild(background)
        self.addChild(newGameButton)
        self.addChild(continueButton)
        self.addChild(optionButton)
        self.addChild(bestiaryButton)
        self.addChild(creditsButton)
    }// fine didMove
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
           if let touch = touches.first {
               let pos = touch.location(in: self)
               let node = self.atPoint(pos)

            if node == newGameButton {
                if view != nil {
                    let transition:SKTransition = SKTransition.fade(withDuration: 1)
                    let scene:SKScene = SceneLevel(size: self.size)
                    self.view?.presentScene(scene, transition: transition)
                }
            }//NEW GAME SCENE
            else if node == optionButton{
                if view != nil {
                    let transition:SKTransition = SKTransition.moveIn(with: .right, duration: 0.5)
                    let scene:SKScene = OptionScene(size: self.size)
                    self.view?.presentScene(scene, transition: transition)
                }
            }//OPTION SCENE
            else if node == bestiaryButton{
                if view != nil {
                    let transition:SKTransition = SKTransition.moveIn(with: .left, duration: 0.5)
                    let scene:SKScene = BestiaryScene(size: self.size)
                    self.view?.presentScene(scene, transition: transition)
                }
            }//BESTIARY SCENE
            else if node == creditsButton{
                if view != nil {
                    let transition:SKTransition = SKTransition.moveIn(with: .down, duration: 0.5)
                    let scene:SKScene = CreditsScene(size: self.size)
                    self.view?.presentScene(scene, transition: transition)
                }
            }//CREDITS SCENE
            
            
            
           }// fine if let touch
       }//fine touchbegan
    
}
