//
//  SceneLevel.swift
//  Bacteriophage
//
//  Created by Paolo Buia on 08/05/2020.
//  Copyright © 2020 InDaCave Studios. All rights reserved.
//

import SpriteKit
import GameplayKit

class SceneLevel: SKScene {
    var viewBounds: CGSize?
    var hero = Hero()
    var isCharacterMoving = false //bool
    let velocityMultiplier: CGFloat = 0.12 //velocità del player
    let gameLayer = SKNode() // il layer del gioco
    let settingLayer = SKNode() //layer settings
    var livesArray : [SKSpriteNode]?
    var attackButton = SKSpriteNode()
    var enemyARandom = SKSpriteNode()
    
    var portal = Portal()
    
    //TEXTURE DEL JOYSTICK
    lazy var analogJoystick: TLAnalogJoystick = {
        let js = TLAnalogJoystick(withDiameter: 100)
        js.base.image = UIImage(named: "substrate")
        js.handle.image = UIImage(named: "joystick")
        js.zPosition = 3
        return js
    }()
    
    //LA CAMERA NODE
    lazy var cameraNode : SKCameraNode = {
        let node = SKCameraNode()
        camera = node
        //      la camera segue la posizione dell'hero
        node.position.x = hero.position.x
        node.position.y = hero.position.y
        
        //        node.position.x = self.frame.size.width / 2
        //        node.position.y = self.frame.size.height / 2
        return node
    }()
    
    //AREA DOVE PUO' ESSERE GENERATO IL JOYSTICK
    lazy var moveJoystickHiddenArea : TLAnalogJoystickHiddenArea = {
        let area = TLAnalogJoystickHiddenArea(rect: CGRect(x: 0.0, y: 0.0, width: self.frame.width, height: 10000))
        area.strokeColor = UIColor.gray.withAlphaComponent(0)
        return area
    }()
    
    override init(size: CGSize){
        super.init(size: size)
        viewBounds = size
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMove(to view: SKView) {
        physicsWorld.contactDelegate = self
        addRandomBackground() //setta un background a random
        addRandomMobsInInterval(min: 4,max: 4) //setta i mob a random
        addHero() //setta l'eroe
        manageJoystick() //gestione joystick
        setupCameraNode() //setta la camera
        setupPlayableArea() //setta la playable area
        setupLifeHero() //setta la vita dell'eroe
        setupPortal() //setta il portale verso il prossimo stage
        setupAttackButton() //setta il bottone d'attacco
        
//        setupEnemyArandom()
        
        addChild(gameLayer)
    }//fine did move
    
    override func update(_ currentTime: TimeInterval) {
        
        moveCameraInARange()
//        scovato memory leak
//        let distance = hero.position.distanceFromCGPoint(point: enemyARandom.position)
//        if distance < 200{
//            attackButton.texture = SKTexture(imageNamed: "attackButton")
//        }
//        else{
//            attackButton.texture = SKTexture(imageNamed: "attackButtonInactive")
//        }
    }// fine update
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let pos = touch.location(in: self)
            let node = self.atPoint(pos)
            
            if node == attackButton{
                hero.attack()
            }
        }//fine if let touch
    }//fine touchesBegan
    
    func setupEnemyArandom(){
        enemyARandom = Mob1()
        enemyARandom.position = CGPoint(x: self.frame.size.width/2, y: 650)
        enemyARandom.zPosition = 100
        enemyARandom.setScale(0.6)
        gameLayer.addChild(enemyARandom)
    }//fine setuEnemyArandom
    

    func setupPlayableArea(){//CREA L'AREA GIOCABILE
        let playableArea = SKPhysicsBody(edgeLoopFrom: CGRect(x: 0.0, y: 0.0, width: self.frame.width, height: 1810))
        self.physicsBody = playableArea
    }// fine setup playable area
    
    func setupAttackButton(){//SETTA IL BOTTONE D'ATTACCO
        attackButton = SKSpriteNode(imageNamed: "attackButton")
        attackButton.zPosition = 101
        attackButton.position = CGPoint(x: (size.width)*0.3, y: size.height/20 - 400)
        attackButton.setScale(0.5)
        cameraNode.addChild(attackButton)
    }//fine setupAttackButton
    
    func attackButtonPressed(){
        let enemyNode = Enemy()
        let joint = SKPhysicsJointPin.joint(withBodyA: hero.physicsBody!, bodyB: enemyNode.physicsBody!, anchor: CGPoint(x: hero.frame.maxX, y: enemyNode.frame.minY))
        self.physicsWorld.add(joint)
    }//fine attackButtonPressed
    
    //setup eroe
    func addHero(){
        let startingXHeroPosition = self.frame.size.width / 2
        let startingYHeroPosition = self.frame.size.height / 2
        hero.position = CGPoint(x: startingXHeroPosition, y: startingYHeroPosition)
        hero.zPosition = 5
        addChild(hero)
        animateHero(spriteNode: hero, arrayFrames: hero.idleFrames)
    }
    
    //aggiunge un random enemy
    func addRandomMobsInInterval(min: UInt32, max: UInt32){
        let numRand = randomNumber(min: min, max: max)
        for _ in 1...numRand{
            addRandomMob()
        }
    }
    
    func addRandomMob(){
        let arrayMobs: [Mob] = [Mob1(scene: self), Mob1(scene: self), Mob1(scene: self)]
        let elem = randomMobFrom(arrayObj: arrayMobs)
        let elemNode = elem.copy() as! Mob
        elemNode.zPosition = 5
        
        elemNode.position = randomPosition()
        elemNode.setScale(CGFloat(randomNumber(min: 3, max: 5))*0.1)
        
        //lo aggiunge come nodo e lo anima all'infinito
        animate(spriteNode: elemNode, arrayFrames: elemNode.idleFrames)
        
        addChild(elemNode)
        elemNode.move(borders: viewBounds!)
        
    }//fine add random enemy
    
    //aggiunge un background random
    func addRandomBackground(){
//        let randomBackground = randomStringFrom(arrayString: arrayBackground)
//        let backgroundNode = Node(folderNamed: randomBackground, frameNamed: randomBackground)
        let backgroundNode = SKSpriteNode(imageNamed: "Bg")
        backgroundNode.zPosition = -10
        backgroundNode.position = CGPoint(x: size.width/2 , y: size.height)
        backgroundNode.setScale(1)
        
        //       lo aggiunge come nodo e lo anima all'infinito
//        animate(spriteNode: backgroundNode, arrayFrames: backgroundNode.idleFrames)
        addChild(backgroundNode)
    }//fine add random background
    
    func randomPosition() -> CGPoint {
        let xPosition = CGFloat(arc4random_uniform(UInt32((view?.bounds.maxX)! + 1)))
        let yPosition = CGFloat(arc4random_uniform(UInt32((view?.bounds.maxY)! + 1)))
        return CGPoint(x: xPosition, y: yPosition)
    }//fine random position
    
    
    
    // azioni del joystick
    private func joystickActionSettings(){
        analogJoystick.on(.begin) { (joystick) in
            self.isCharacterMoving = true
        }
        analogJoystick.on(.move) { [unowned self] joystick in
            
            if self.isCharacterMoving == true{
                //AGGIORNAMENTO POSIZIONE DEL PERSONAGGIO
                self.hero.position = CGPoint(x: self.hero.position.x + (joystick.velocity.x * self.velocityMultiplier), y: self.hero.position.y + (joystick.velocity.y * self.velocityMultiplier))
                if (!self.hero.isAttacking) {
                    self.hero.zRotation = joystick.angular
                }
                
                
            }//fine dell'if
        }
        analogJoystick.on(.end) { (joystick) in
            //            bloccava tutte le azioni dell'hero
            //            self.hero.removeAllActions()
        }
    }//fine joystickActionSettings
    
    func manageJoystick(){
        moveJoystickHiddenArea.joystick = analogJoystick
        moveJoystickHiddenArea.zPosition = 20
        analogJoystick.isMoveable = true
        
        gameLayer.addChild(moveJoystickHiddenArea)
        
        joystickActionSettings()
    }//fine manageJoystick
    
    
    func setupCameraNode(){
        gameLayer.addChild(cameraNode)
        cameraNode.addChild(settingLayer)
        
    }//fine setupCameraNode
    
    //muove la camera in un certo range
    func moveCameraInARange(){
        // se l'eroe scende di un certo tot la camera non lo segue
        // se l'eroe sale di un certo tot la camera non lo segue
        let startingYHeroPosition = self.frame.size.height / 2
        if hero.position.y > startingYHeroPosition && hero.position.y < 1370.0{
            cameraNode.position.y = hero.position.y
        }
    }// fine move camera in a range
    
    
    func setupLifeHero(){
        livesArray = [SKSpriteNode]()
        for live in 1 ... hero.hp {
            let liveNode = SKSpriteNode(imageNamed: "heart")
            liveNode.position = CGPoint(x: (size.width)*0.4, y: size.height/8 - CGFloat(9 - live) * liveNode.size.width/2)
            liveNode.setScale(0.4)
            liveNode.zPosition = 100
            cameraNode.addChild(liveNode)
            livesArray!.append(liveNode)
        }
    }
    
//    add a node heart to te scene
    func addLifeHero(){
        livesArray = [SKSpriteNode]()
        for live in 1 ... hero.hp {
            let liveNode = SKSpriteNode(imageNamed: "heart")
            liveNode.position = CGPoint(x: (size.width)*0.4, y: size.height/8 - CGFloat(9 - live) * liveNode.size.width/2)
            liveNode.setScale(0.4)
            liveNode.zPosition = 100
            cameraNode.addChild(liveNode)
            livesArray!.append(liveNode)
        }
    }
    
    func removeLife(){
        livesArray?.popLast()?.removeFromParent()
    }
    
    func setupPortal(){
        portal.zPosition = 4
        
        portal.position = CGPoint(x: self.frame.size.width/2, y: self.frame.size.height*1.9)
        
        //      lo aggiunge come nodo e lo anima all'infinito
        animate(spriteNode: portal, arrayFrames: portal.idleFrames)
        
        addChild(portal)
        
    }
    
    func presentNewScene(oldScene: SKScene, newScene: SKScene){
        oldScene.removeAllActions()
        oldScene.removeAllChildren()
        if oldScene.view != nil {
            let transition:SKTransition = SKTransition.fade(withDuration: 1)
            let scene:SKScene = newScene
            self.view?.presentScene(scene, transition: transition)
        }
        
    }
//    
//    deinit {
//        print("hero deallocato")
//        print("camera deallocato")
//        print("joystick deallocato")
//    }
    
}


extension SceneLevel: SKPhysicsContactDelegate {
    func didBegin(_ contact: SKPhysicsContact) {
        collisionHeroMobs(contact: contact)
        contactHeroPortal(contact: contact)
        contactHeroWeakSpot(contact: contact)
        contactHeroPowerUp(contact: contact)
    }
    
    
    func enemyDidCollideWithHero(enemy: Enemy, hero: Hero) {
        print("enemy hit hero")
        hero.hp -= 1
        removeLife()
        //        quando colpito il corpo si colora di rosso istantaneamente (duration=0 ed il color blender serve a rendere rossa la texture altrimenti non si vedrebbe niente) mantengo per 0.05 sec il colore rosso e riporto la textur a colore normale
        hero.run(SKAction.sequence([SKAction.colorize(with: .red, colorBlendFactor: 1, duration: 0),
                                    SKAction.wait(forDuration: 0.05),
                                    SKAction.colorize(withColorBlendFactor: 0, duration: 0)]))
        if hero.hp <= 0 {
            hero.die()
            presentNewScene(oldScene: self, newScene: GameOverScene(size: self.size))
            return
        }
        
    }
    
    func portalDidCollideWithHero(portal: Portal, hero: Hero) {
        print("Hero touched the portal")
        presentNewScene(oldScene: self, newScene: SceneLevel(size: self.size))

    }
    
    
    func collisionHeroMobs(contact: SKPhysicsContact) {
        
        var firstBody: SKPhysicsBody?
        var secondBody: SKPhysicsBody?
        if contact.bodyA.categoryBitMask == PhysicsCategory.Hero && contact.bodyB.categoryBitMask == PhysicsCategory.Enemy {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else if contact.bodyB.categoryBitMask == PhysicsCategory.Hero && contact.bodyA.categoryBitMask == PhysicsCategory.Enemy {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        // 2
        if firstBody != nil && secondBody != nil{
            if ((firstBody!.categoryBitMask & PhysicsCategory.Hero != 0) &&
                (secondBody!.categoryBitMask & PhysicsCategory.Enemy != 0)) {
                if let hero = firstBody!.node as? SKSpriteNode,
                    let enemy = secondBody!.node as? SKSpriteNode {
                    enemyDidCollideWithHero(enemy: enemy as! Enemy, hero: hero as! Hero)
                }
            }
        }
    }
    
    
    func contactHeroPortal(contact: SKPhysicsContact) {
        
        var firstBody: SKPhysicsBody?
        var secondBody: SKPhysicsBody?
        if contact.bodyA.categoryBitMask == PhysicsCategory.Hero && contact.bodyB.categoryBitMask == PhysicsCategory.Portal {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else if contact.bodyB.categoryBitMask == PhysicsCategory.Hero && contact.bodyA.categoryBitMask == PhysicsCategory.Portal {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        // 2
        if firstBody != nil && secondBody != nil{
            if ((firstBody!.categoryBitMask & PhysicsCategory.Hero != 0) &&
                (secondBody!.categoryBitMask & PhysicsCategory.Portal != 0)) {
                if let hero = firstBody!.node as? SKSpriteNode,
                    let portal = secondBody!.node as? SKSpriteNode {
                    portalDidCollideWithHero(portal: portal as! Portal, hero: hero as! Hero)
                }
            }
        }
    }
    
    func contactHeroPowerUp(contact: SKPhysicsContact) {
           
           var firstBody: SKPhysicsBody?
           var secondBody: SKPhysicsBody?
           if contact.bodyA.categoryBitMask == PhysicsCategory.Hero && contact.bodyB.categoryBitMask == PhysicsCategory.PowerUp {
               firstBody = contact.bodyA
               secondBody = contact.bodyB
           } else if contact.bodyB.categoryBitMask == PhysicsCategory.Hero && contact.bodyA.categoryBitMask == PhysicsCategory.PowerUp {
               firstBody = contact.bodyB
               secondBody = contact.bodyA
           }
           
           // 2
           if firstBody != nil && secondBody != nil{
               if ((firstBody!.categoryBitMask & PhysicsCategory.Hero != 0) &&
                   (secondBody!.categoryBitMask & PhysicsCategory.Portal != 0)) {
                   if let hero = firstBody!.node as? SKSpriteNode,
                       let powerUp = secondBody!.node as? SKSpriteNode {
                       powerUpDidCollideWithHero(powerUp: powerUp as! PowerUp, hero: hero as! Hero)
                   }
               }
           }
       }
    
    func powerUpDidCollideWithHero(powerUp: PowerUp, hero: Hero) {
        powerUp.removeFromParent()
        switch powerUp.name{
        case "hp":
            print("hero hit powerup hp")
            hero.hp+=1
            addLifeHero()
        case "speed":
            print("hero hit powerup speed")
        default:
            print("_unknown")
        }
        
    }
    
    func contactHeroWeakSpot(contact: SKPhysicsContact) {
        
        var firstBody: SKPhysicsBody?
        var secondBody: SKPhysicsBody?
        if contact.bodyA.categoryBitMask == PhysicsCategory.Hero && contact.bodyB.categoryBitMask == PhysicsCategory.WeakSpot {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else if contact.bodyB.categoryBitMask == PhysicsCategory.Hero && contact.bodyA.categoryBitMask == PhysicsCategory.WeakSpot {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        // 2
        if firstBody != nil && secondBody != nil{
            if ((firstBody!.categoryBitMask & PhysicsCategory.Hero != 0) &&
                (secondBody!.categoryBitMask & PhysicsCategory.WeakSpot != 0)) {
                if let hero = firstBody!.node as? Hero,
                    let weakSpot = secondBody!.node as? WeakSpot {
                    if hero.isAttacking{
                        weakSpotDidCollideWithHero(weakSpot: weakSpot, hero: hero)
                    }
                }
            }
        }
    }
    
    func weakSpotDidCollideWithHero(weakSpot: WeakSpot, hero: Hero) {
        print("hero hit weakspot")
        
        //        quando colpito il corpo si colora di rosso istantaneamente (duration=0 ed il color blender serve a rendere rossa la texture altrimenti non si vedrebbe niente) mantengo per 0.05 sec il colore rosso e riporto la textur a colore normale
        weakSpot.father?.run(SKAction.sequence([SKAction.colorize(with: .red, colorBlendFactor: 1, duration: 0),
                                    SKAction.wait(forDuration: 0.05),
                                    SKAction.colorize(withColorBlendFactor: 0, duration: 0)]))
        weakSpot.father?.die()
        
    }
}
