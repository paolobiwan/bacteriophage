//
//  GameOverScene.swift
//  Bacteriophage
//
//  Created by Girolamo Pinto on 10/05/2020.
//  Copyright © 2020 InDaCave Studios. All rights reserved.
//

import SpriteKit

class GameOverScene : SKScene{
    var gameOverLabel = SKLabelNode()
    var homeButton = SKSpriteNode()
    var replayButton = SKSpriteNode()
    
    override func didMove(to view: SKView) {
        self.backgroundColor = .black
        self.backgroundColor.withAlphaComponent(0.5)
        
        setupGameOverLabel()
        setupReplayButton()
        setupHomeButton()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first{
            let pos = touch.location(in: self)
            let node = self.atPoint(pos)
            
            if node == replayButton{
                if view != nil {
                    let transition:SKTransition = SKTransition.fade(withDuration: 1)
                    let scene:SKScene = SceneLevel(size: self.size)
                    self.view?.presentScene(scene, transition: transition)
                }
            }//replay button
            
            if node == homeButton{
                if view != nil {
                    let transition:SKTransition = SKTransition.fade(withDuration: 1)
                    let scene:SKScene = MenuScene(size: self.size)
                    self.view?.presentScene(scene, transition: transition)
                }
            }
        }
    }
    
    func setupGameOverLabel(){
        gameOverLabel = SKLabelNode(text: "Game Over")
        gameOverLabel.fontSize = 60
        gameOverLabel.zPosition = 1
        gameOverLabel.position = CGPoint(x: self.frame.size.width/2, y: self.frame.size.height/1.2)
        self.addChild(gameOverLabel)
    }
    
    func setupReplayButton(){
        replayButton = SKSpriteNode(imageNamed: "replay")
        replayButton.setScale(0.5)
        replayButton.zPosition = 1
        replayButton.position = CGPoint(x: self.frame.width/1.5, y: self.frame.size.height/2)
        self.addChild(replayButton)
    }
    
    func setupHomeButton(){
        homeButton = SKSpriteNode(imageNamed: "homeButton")
        homeButton.color = .white
        homeButton.setScale(0.5)
        homeButton.zPosition = 1
        homeButton.position = CGPoint(x: self.frame.width/2.8, y: self.frame.size.height/2)
        self.addChild(homeButton)
    }
}
