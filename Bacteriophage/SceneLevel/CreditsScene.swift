//
//  CreditsScene.swift
//  Bacteriophage
//
//  Created by Antonio Giugliano on 10/05/2020.
//  Copyright © 2020 InDaCave Studios. All rights reserved.
//

import Foundation
import SpriteKit
import UIKit
class CreditsScene: SKScene{
    var background = SKSpriteNode(imageNamed: "kurtz")
    var backButton = SKSpriteNode(imageNamed: "Back")
    var logo = SKSpriteNode(imageNamed: "Credits")
    var label = SKLabelNode(fontNamed: "SF Compact Rounded")
    override func didMove(to view: SKView) {
        //SETTAGGIO BACKGROUND
        background.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
        background.zPosition = -1
        background.setScale(1.15)
        //SETTAGGIO LOGO
        logo.size = CGSize(width: 293, height: 68)
        logo.setScale(0.7)
        logo.position = CGPoint(x: frame.midX, y: 0.85*frame.maxY)
        //SETTAGGIO PULSANTE BACK
        backButton.position = CGPoint(x: frame.midX, y: 0.10*frame.maxY)
        backButton.size = CGSize(width: 210, height: 70)
        backButton.setScale(0.5)
        //LABEL
        label.position = CGPoint(x: frame.midX, y: frame.midY)
        label.fontColor = UIColor(named: "Credits Color")
        label.fontSize = 30
        label.numberOfLines = 40
        label.text = "\nInDaCave Studios \n\nPaolo Buia \nMichael Capasso \nMichelangelo Di Giacomo \nAntonio Giugliano \nGiammarco Mastronardi \nGirolamo Pinto \n\nOur special thanks go to \nTiago Pereira \n\nFor the guidance he \ngave us in all the \nstages of development"

        label.horizontalAlignmentMode = .center 
        label.verticalAlignmentMode = .center
        
        
        addChild(label)
        addChild(logo)
        addChild(background)
        addChild(backButton)
    }//fine didMove
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let pos = touch.location(in: self)
            let node = self.atPoint(pos)
            if node == backButton {
                if view != nil {
                    let transition:SKTransition = SKTransition.moveIn(with: .up, duration: 0.5)
                    let scene:SKScene = MenuScene(size: self.size)
                    self.view?.presentScene(scene, transition: transition)
                }
            }// RITORNO ALLA MENU SCENE
        }//fine if let touch
        
}// fine touchesBegan
    
}//fine class CreditsScene
