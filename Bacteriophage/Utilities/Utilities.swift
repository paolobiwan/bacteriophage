//
//  Utilities.swift
//  Bacteriophage
//
//  Created by Paolo Buia on 08/05/2020.
//  Copyright © 2020 InDaCave Studios. All rights reserved.
//

import SpriteKit


var arrayBackground = ["celihackBGFrames", "violetBGFrames"]


func setAnimationFrames(folderNamed: String, frameNamed: String) -> [SKTexture] {
    let animatedAtlas = SKTextureAtlas(named: folderNamed)
    var arrayFrames: [SKTexture] = []

    let numImages = animatedAtlas.textureNames.count
    for i in 1...numImages {
    let textureName = "\(frameNamed)\(i)"
    arrayFrames.append(animatedAtlas.textureNamed(textureName))
    }
    return arrayFrames
}
//get array string of elements in foldername passed
func elementsDirectory(folderName: String)-> [String]  {
    let resourcePath = NSURL(string: Bundle.main.resourcePath!)?.appendingPathComponent(folderName)
    let resourcesContent = try! FileManager().contentsOfDirectory(at: resourcePath!, includingPropertiesForKeys: nil, options: FileManager.DirectoryEnumerationOptions.skipsHiddenFiles)
    var array = [String]()
    for url in resourcesContent {
        let str: String = url.lastPathComponent
        print(url)
        array.append(str)
    }
    
    return array
}

//get a random string from array of strings
func randomStringFrom(arrayString: [String])-> String{
    let lower : UInt32 = 0
    let upper : UInt32 = UInt32(arrayString.count)
    let randomNumber = Int(arc4random_uniform(upper - lower) + lower)
    
    return arrayString[randomNumber]
}

//PRENDE UN MOB RNADOM TRA QUELLI DISPONIBILI
func randomMobFrom(arrayObj: [Mob])-> Mob{
    let lower : UInt32 = 0
    let upper : UInt32 = UInt32(arrayObj.count)
    let randomNumber = Int(arc4random_uniform(upper - lower) + lower)
    
    return arrayObj[randomNumber]
}

func setRandomAnimationFrames(arrayString: [String])-> [SKTexture]{
    let randElem = randomStringFrom(arrayString: arrayString)
    return setAnimationFrames(folderNamed: randElem, frameNamed: randElem)
}

func animate(spriteNode: SKSpriteNode, arrayFrames: [SKTexture] ) {
spriteNode.run(SKAction.repeatForever(
    SKAction.animate(with: arrayFrames,
                     timePerFrame: 0.1,
                     resize: false,
    restore: true)))
}

func animateHero(spriteNode: SKSpriteNode, arrayFrames: [SKTexture] ) {
spriteNode.run(SKAction.repeatForever(
    SKAction.animate(with: arrayFrames,
                     timePerFrame: 0.025,
                     resize: false,
    restore: true)), withKey: "idle")
}

//anima a velocità superiore i frame
func animateQuick(spriteNode: SKSpriteNode, arrayFrames: [SKTexture] ) {
spriteNode.run(SKAction.repeatForever(
    SKAction.animate(with: arrayFrames,
                     timePerFrame: 0.025,
                     resize: false,
                     restore: true)))
}

func animateWithSpeed(spriteNode: SKSpriteNode, arrayFrames: [SKTexture] , speed: TimeInterval) {
spriteNode.run(SKAction.repeatForever(
    SKAction.animate(with: arrayFrames,
                     timePerFrame: speed,
                     resize: false,
                     restore: true)))
}

func animateWithSpeedOneTime(spriteNode: SKSpriteNode, arrayFrames: [SKTexture] , speed: TimeInterval) {
    spriteNode.run(
        SKAction.animate(
            with: arrayFrames,
            timePerFrame: speed,
            resize: false,
            restore: true
        )
    )
}


func randomNumber(min: UInt32, max: UInt32)->UInt32{
    let lower : UInt32 = min
    let upper : UInt32 = max
    let randomNumber = arc4random_uniform(upper - lower) + lower
    return randomNumber
}


 
struct PhysicsCategory {
    static let none        : UInt32 = 0
    static let all         : UInt32 = UInt32.max
    static let Hero        : UInt32 = 0b1
    static let Enemy       : UInt32 = 0b10
    static let Projectile  : UInt32 = 0b11
    static let Portal      : UInt32 = 0b100
    static let WeakSpot    : UInt32 = 0b101
    static let PowerUp     : UInt32 = 0b110
    static let spell       : UInt32 = 0b111
    static let enemy       : UInt32 = 0b1000
    static let shieldPlayer: UInt32 = 0b1001
}

extension CGPoint {
    func distanceFromCGPoint(point:CGPoint)->CGFloat{
        return sqrt(pow(self.x - point.x,2) + pow(self.y - point.y,2))
    }
    
    func length() -> CGFloat {
       return sqrt(x*x + y*y)
     }
    
    static func /(point: CGPoint, scalar: CGFloat) -> CGPoint {
      return CGPoint(x: point.x / scalar, y: point.y / scalar)
    }

    
    func normalized() -> CGPoint {
        return self / length()
    }
}

extension CGVector {
    func speed() -> CGFloat {
        return sqrt(dx*dx+dy*dy)
    }
    func angle() -> CGFloat {
        return atan2(dy, dx)
    }
}
