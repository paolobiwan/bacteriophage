//
//  Node.swift
//  Bacteriophage
//
//  Created by Girolamo Pinto on 07/05/2020.
//  Copyright © 2020 InDaCave Studios. All rights reserved.
//

import SpriteKit

class Node: SKSpriteNode{
    var idleFrames: [SKTexture]=[]
    var movFrames: [SKTexture] = []
    
    init(folderNamed: String, frameNamed: String) {
        idleFrames = setAnimationFrames(folderNamed: folderNamed, frameNamed: frameNamed)
        let firstFrameTexture = idleFrames[0]
        super.init(texture: firstFrameTexture, color: UIColor.clear, size: firstFrameTexture.size())
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize){
        super.init(texture: texture, color: color, size: size)
    }
    
    

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
