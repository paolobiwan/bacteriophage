//
//  HpUp.swift
//  Bacteriophage
//
//  Created by Paolo Buia on 15/05/2020.
//  Copyright © 2020 InDaCave Studios. All rights reserved.
//


import SpriteKit

class HpUp: PowerUp{
    
    override init(folderNamed: String, frameNamed: String) {
        super.init(folderNamed: folderNamed, frameNamed: frameNamed)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize){
        super.init(texture: texture, color: color, size: size)
    }
    
    init(){
        super.init(folderNamed: "dnaPink", frameNamed: "dnaPink")
        name = "hp"
        let size = CGSize(width: self.size.width/2, height: self.size.height)
        let center = CGPoint(x: position.x, y: position.y)
        self.physicsBody = SKPhysicsBody(rectangleOf: size, center: center)
        setScale(0.3)
        self.physicsBody?.isDynamic = false
        self.physicsBody?.affectedByGravity = false
        self.physicsBody?.categoryBitMask = PhysicsCategory.PowerUp
        self.physicsBody?.contactTestBitMask = PhysicsCategory.Hero
        animate(spriteNode: self, arrayFrames: idleFrames)
    }
    
}
