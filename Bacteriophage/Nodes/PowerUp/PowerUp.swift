//
//  PowerUp.swift
//  Bacteriophage
//
//  Created by Paolo Buia on 14/05/2020.
//  Copyright © 2020 InDaCave Studios. All rights reserved.
//


import SpriteKit

class PowerUp: Node{
    var contactFrames: [SKTexture] = []
    
    override init(folderNamed: String, frameNamed: String) {
        super.init(folderNamed: folderNamed, frameNamed: frameNamed)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize){
        super.init(texture: texture, color: color, size: size)
    }
    
    
}
