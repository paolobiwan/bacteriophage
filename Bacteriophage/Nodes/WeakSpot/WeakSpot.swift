//
//  WeakSpot.swift
//  Bacteriophage
//
//  Created by Paolo Buia on 14/05/2020.
//  Copyright © 2020 InDaCave Studios. All rights reserved.
//

import SpriteKit

class WeakSpot: Node{
    var father: Mob?
    override init(folderNamed: String, frameNamed: String) {
        super.init(folderNamed: folderNamed, frameNamed: frameNamed)
        self.physicsBody = SKPhysicsBody(circleOfRadius: self.size.height/4)
        self.physicsBody?.isDynamic = false
        self.physicsBody?.affectedByGravity = false
        self.physicsBody?.categoryBitMask = PhysicsCategory.WeakSpot
        self.physicsBody?.contactTestBitMask = PhysicsCategory.Hero
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize){
        super.init(texture: texture, color: color, size: size)
    }
    
    init(father: Mob){
        super.init(folderNamed: "weakSpot", frameNamed: "weakSpot")
        self.physicsBody = SKPhysicsBody(circleOfRadius: self.size.height/3)
        self.physicsBody?.isDynamic = false
        self.physicsBody?.affectedByGravity = false
        self.physicsBody?.categoryBitMask = PhysicsCategory.WeakSpot
        self.physicsBody?.contactTestBitMask = PhysicsCategory.Hero
        self.father = father
        print("allocated weakspot")
    }
    
    deinit{
        print("deallocated weakspot")
    }
    
    
}
