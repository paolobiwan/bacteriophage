//
//  Hero.swift
//  Bacteriophage
//
//  Created by Paolo Buia on 08/05/2020.
//  Copyright © 2020 InDaCave Studios. All rights reserved.
//

import SpriteKit

class Hero: Character{
    var isAttacking = false
    
    
    override init(folderNamed: String, frameNamed: String) {
        super.init(folderNamed: folderNamed, frameNamed: frameNamed)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize){
        super.init(texture: texture, color: color, size: size)
        self.name = "Hero"
    }
    
    init(){
        super.init(folderNamed: "fagoMovC", frameNamed: "fagoMovC")
        hp = 4
        setScale(0.25)
        let size = CGSize(width: self.size.width/13, height: self.size.height/2.4)
        let center = CGPoint(x: position.x, y: position.y)
        self.physicsBody = SKPhysicsBody(rectangleOf: size, center: center)
        
        self.physicsBody?.isDynamic = true
        self.physicsBody?.affectedByGravity = false
        self.physicsBody?.categoryBitMask = PhysicsCategory.Hero
        self.physicsBody?.contactTestBitMask = PhysicsCategory.Enemy
        atkFrames=setAnimationFrames(folderNamed: "fagoInjectC", frameNamed: "fagoInjectC")
        movFrames=setAnimationFrames(folderNamed: "fagoMovC", frameNamed: "fagoMovC")
        print("allocated hero")
    }
    
    deinit{
        print("deallocated hero")
    }
    
    override func attack(){
        isAttacking = true
        removeAction(forKey: "idle")
        
        zRotation=zRotation+CGFloat.pi
        animateWithSpeedOneTime(spriteNode: self, arrayFrames: atkFrames, speed: 0.003)
        let queueA = DispatchQueue.init(label: "it.xcoding.queueA", qos: .userInitiated)

        let time: DispatchTime = DispatchTime.now() + DispatchTimeInterval.seconds(1) - 0.5

        queueA.asyncAfter(deadline: time) {
            self.isAttacking = false
            animateHero(spriteNode: self, arrayFrames: self.idleFrames)
        }
    }
    
    
    
    func attackD(joystick : TLAnalogJoystick){
        let movTime = 4.5
        let newPosition = CGPoint(x: position.x + (joystick.velocity.x * CGFloat(movTime)), y: position.y + (joystick.velocity.y * CGFloat(movTime)))
        let actionMove = SKAction.move(to: newPosition, duration: 0.2)
        
        run(actionMove)
        isAttacking = true
        removeAction(forKey: "idle")
        
        zRotation=zRotation+CGFloat.pi
        animateWithSpeedOneTime(spriteNode: self, arrayFrames: atkFrames, speed: 0.003)
        let queueA = DispatchQueue.init(label: "it.xcoding.queueA", qos: .userInitiated)

        let time: DispatchTime = DispatchTime.now() + DispatchTimeInterval.seconds(1) - 0.4

        queueA.asyncAfter(deadline: time) {
            self.isAttacking = false
            animateHero(spriteNode: self, arrayFrames: self.idleFrames)
        }
    }
}


