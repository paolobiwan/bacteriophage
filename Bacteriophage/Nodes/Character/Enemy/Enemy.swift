//
//  Enemy.swift
//  phage
//
//  Created by Paolo Buia on 30/04/2020.
//  Copyright © 2020 Paolo Buia. All rights reserved.
//

import SpriteKit

class Enemy: Character{
    var weakSpot : Bool = false
    var type: UInt = 0
    
    override init(folderNamed: String, frameNamed: String) {
        super.init(folderNamed: folderNamed, frameNamed: frameNamed)
        self.physicsBody = SKPhysicsBody(circleOfRadius: self.size.height/3)
        self.physicsBody?.isDynamic = false
        self.physicsBody?.affectedByGravity = false
        self.physicsBody?.categoryBitMask = PhysicsCategory.Enemy
        self.physicsBody?.contactTestBitMask = PhysicsCategory.Hero
        print("allocated enemy")    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize){
        super.init(texture: texture, color: color, size: size)
    }
    func ableWeakSpot(){
        weakSpot=true
    }
    
    func unAbleWeakSpot(){
        weakSpot=false
    }
    
    deinit{
        print("deallocated enemy")
    }
    
    
}
