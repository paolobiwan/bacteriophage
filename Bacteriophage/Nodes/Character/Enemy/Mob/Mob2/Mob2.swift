//
//  Mob2.swift
//  Bacteriophage
//
//  Created by Paolo Buia on 08/05/2020.
//  Copyright © 2020 InDaCave Studios. All rights reserved.
//

import SpriteKit

class Mob2: Mob{
    
    override init(folderNamed: String, frameNamed: String) {
        super.init(folderNamed: folderNamed, frameNamed: frameNamed)
        type = 2
        hp = 10
    }
    
     init(){
        super.init(folderNamed: "virust", frameNamed: "virust")
        super.size = CGSize(width: 576, height: 89)
        
        
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize){
        super.init(texture: texture, color: color, size: size)
    }
    
    
    override func move(borders: CGSize){
        let firstPosX = position.x
        let firstPosY = position.y
        let movTime = arc4random_uniform(4) + arc4random_uniform(8);
        let viewW = borders.width
        let viewH = borders.height
        
        
        let actionMove = SKAction.move(to: CGPoint(x: viewW/3, y: viewH/4), duration: TimeInterval(movTime))
        let actionMoveDone = SKAction.move(to: CGPoint(x: firstPosX, y: firstPosY), duration: TimeInterval(movTime))
        
        let action = SKAction.sequence([actionMove, actionMoveDone])
        run(SKAction.repeatForever(action))
    }
    
    
    
}

