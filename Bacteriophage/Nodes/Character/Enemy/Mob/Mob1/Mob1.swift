//
//  Mob1.swift
//  Bacteriophage
//
//  Created by Paolo Buia on 08/05/2020.
//  Copyright © 2020 InDaCave Studios. All rights reserved.
//

import SpriteKit

class Mob1: Mob{
    var sceneLvl: SceneLevel?
    override init(folderNamed: String, frameNamed: String) {
        super.init(folderNamed: folderNamed, frameNamed: frameNamed)
        
    }
    
     init(){
        super.init(folderNamed: "Mob1IdleFrames", frameNamed: "Mob1IdleFrames")
        type = 1
        hp = 10
        let weakSpotNode = WeakSpot(father: self)
        weakSpotNode.zPosition=zPosition+1
        weakSpotNode.position.x = position.x + 130
        addChild(weakSpotNode)
    }
    
    init(scene: SceneLevel){
        super.init(folderNamed: "Mob1IdleFrames", frameNamed: "Mob1IdleFrames")
        type = 1
        hp = 10
        let weakSpotNode = WeakSpot(father: self)
        weakSpotNode.zPosition = zPosition
        weakSpotNode.position.x = position.x + 130
        sceneLvl = scene
        addChild(weakSpotNode)
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize){
        super.init(texture: texture, color: color, size: size)
    }
    
    
    override func move(borders: CGSize){
        let firstPosX = position.x
        let firstPosY = position.y
        let movTime = 4;
        let viewW = borders.width
        let viewH = borders.height
        
        
        let actionMove = SKAction.move(to: CGPoint(x: viewW, y: viewH), duration: TimeInterval(movTime))
        let actionMoveDone = SKAction.move(to: CGPoint(x: firstPosX, y: firstPosY), duration: TimeInterval(movTime))
        
        let action = SKAction.sequence([actionMove, actionMoveDone])
        run(SKAction.repeatForever(action))
    }
    
    func drop(powerUp: PowerUp){
        
        powerUp.position = position
        powerUp.zPosition = zPosition+1
        scene?.addChild(powerUp)
        
        print(position)
        print("sto droppando")
    }
    
    override func die(){
        let value = UInt(randomNumber(min: 1, max: 100))
        if value <= 33{
            drop(powerUp: HpUp())
        }else if (value > 33 && value <= 66){
            drop(powerUp: SpeedUp())
        }
        
        super.die()
    }
    
//    func shot(){
//        var proj = Projectile()
//        
//    }
    
}

