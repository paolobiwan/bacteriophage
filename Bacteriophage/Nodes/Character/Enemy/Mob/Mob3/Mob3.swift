//
//  Mob3.swift
//  Bacteriophage
//
//  Created by Paolo Buia on 08/05/2020.
//  Copyright © 2020 InDaCave Studios. All rights reserved.
//


import SpriteKit

class Mob3: Mob{
    
    override init(folderNamed: String, frameNamed: String) {
        super.init(folderNamed: folderNamed, frameNamed: frameNamed)
        type = 2
        hp = 20
    }
    
    init(){
        super.init(folderNamed: "virusz", frameNamed: "virusz")
        
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize){
        super.init(texture: texture, color: color, size: size)
    }
    
    
    override func move(borders: CGSize){
        let firstPosX = position.x
        let firstPosY = position.y
        let movTime = arc4random_uniform(4) + arc4random_uniform(8);
        let viewW = borders.width
        let viewH = borders.height
        
        
//        let spiral = SKAction.spiral(startRadius: size.width / 2,
//                                     endRadius: 0,
//                                     angle: CGFloat(M_PI) * 2,
//                                     centerPoint: node.position,
//                                     duration: 5.0)
//
//        node.runAction(spiral)
        
        let actionMove = SKAction.move(to: CGPoint(x: viewW/2, y: viewH/2), duration: TimeInterval(movTime))
        let actionMoveDone = SKAction.move(to: CGPoint(x: firstPosX, y: firstPosY), duration: TimeInterval(movTime))
        
        let action = SKAction.sequence([actionMove, actionMoveDone])
        run(SKAction.repeatForever(action))
    }
    
    
    
}


