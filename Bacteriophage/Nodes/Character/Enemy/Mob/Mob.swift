//
//  Mob.swift
//  Bacteriophage
//
//  Created by Paolo Buia on 08/05/2020.
//  Copyright © 2020 InDaCave Studios. All rights reserved.
//

import SpriteKit

class Mob: Enemy{
    
    override init(folderNamed: String, frameNamed: String) {
        super.init(folderNamed: folderNamed, frameNamed: frameNamed)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize){
        super.init(texture: texture, color: color, size: size)
    }
    
    func move(borders: CGSize){
        
    }
    
    func drop(){
        
    }
}

