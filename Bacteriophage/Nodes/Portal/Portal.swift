//
//  Portal.swift
//  Bacteriophage
//
//  Created by Paolo Buia on 10/05/2020.
//  Copyright © 2020 InDaCave Studios. All rights reserved.
//

import SpriteKit

class Portal: Node{
    var contactFrames: [SKTexture] = []
    
    override init(folderNamed: String, frameNamed: String) {
        super.init(folderNamed: folderNamed, frameNamed: frameNamed)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize){
        super.init(texture: texture, color: color, size: size)
    }
    
    init(){
            super.init(folderNamed: "portal", frameNamed: "portal")
            self.physicsBody = SKPhysicsBody(circleOfRadius: self.size.height/4)
            self.physicsBody?.isDynamic = false
            self.physicsBody?.affectedByGravity = false
            self.physicsBody?.categoryBitMask = PhysicsCategory.Portal
            self.physicsBody?.contactTestBitMask = PhysicsCategory.Hero
        }
    
}
