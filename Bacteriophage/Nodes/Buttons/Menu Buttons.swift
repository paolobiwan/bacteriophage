//
//  Menu Buttons.swift
//  Bacteriophage
//
//  Created by Antonio Giugliano on 14/05/2020.
//  Copyright © 2020 InDaCave Studios. All rights reserved.
//

import Foundation
import SpriteKit



class MenuButtons : Node{
    
    
    
    init(size: CGSize, texture: SKTexture?, color: UIColor, zPosition: Int){
        super.init(texture: texture, color: color, size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
