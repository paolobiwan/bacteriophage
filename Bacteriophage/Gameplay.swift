//
//  Gameplay.swift
//  Joystick
//
//  Created by Girolamo Pinto on 28/04/2020.
//  Copyright © 2020 Girolamo Pinto. All rights reserved.
//

import UIKit
import SpriteKit

// Girolamo Update per chi legge il codice :
// 1) Ci sono un po' di problemi, la playable area sembra non funzionare
// forse si trova una posizione diversa
// 2) La camera deve seguire il giocatore fin quando va verso l'alto, se va verso il basso, dove finisce il background per intenderci, allora non deve seguire più il player
// 3) Il background basta aumentare l'height e lo si può fare tranquillamente con sketch senza aumentarne la size, altrimenti si bugga tutto.
// Buon Lavoro!!

class Gameplay: SKScene {

    let velocityMultiplier: CGFloat = 0.12
    let heroRadius : CGFloat = 100
    var isCharacterMoving = false

    var backgrounds = [Portrait]()

    enum NodeZPosition: CGFloat{
        case background, hero, joystick
    }

    lazy var background: SKSpriteNode = {
//        questo è il background, centrata in zero e scalata con la dimensione dello schermo
        var sprite = SKSpriteNode(imageNamed: "background")
        sprite.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2)
//        sprite.zPosition = NodeZPosition.background.rawValue
        sprite.scaleTo(screenWidthPercentage: 1.0)
        return sprite
    }()

    lazy var hero: SKSpriteNode = {
//        questo è il batteriofago: è centrato in zero, ma è scalata in base allo schermo?? io non lo farei così
        var sprite = SKSpriteNode(imageNamed: "hero")
        sprite.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
//        sprite.zPosition = NodeZPosition.background.rawValue  così il pg a volte esce ed a volte no, deve essere un valore diverso dal background
        sprite.zPosition = 4 //giusto per non farlo buggare
        sprite.scaleTo(screenWidthPercentage: 0.23)//farei uno scale normale, così siamo limitati alla dimesione dello schermo (ci sta forse no)
        return sprite
    }()

    lazy var analogJoystick: TLAnalogJoystick = {
//        è l'analogico, tutto il funzionamento sta in TLA... stanno definite solo le texture qui, aggiungo zposition perchè a volte non si vede bene il tasto. aggiungo z position uguale al personaggio
        let js = TLAnalogJoystick(withDiameter: 100)
        js.base.image = UIImage(named: "substrate")
        js.handle.image = UIImage(named: "joystick")
        js.zPosition = 4 //4 messo a cazzo
        return js
    }()

    lazy var cameraNode : SKCameraNode = {
       let node = SKCameraNode()
        camera = node
        node.position.x = size.width / 2
        node.position.y = size.height / 2
       return node
    }()

    lazy var moveJoystickHiddenArea : TLAnalogJoystickHiddenArea = {
        let area = TLAnalogJoystickHiddenArea(rect: CGRect(x: 0.0, y: 0.0, width: self.frame.width, height: 10000))
        area.strokeColor = UIColor.gray.withAlphaComponent(1)
        return area
    }()

    override func didMove(to view: SKView) {
        setupBackground()
        setupHero()
        setupCameraNode()
        manageJoystick()
    }

    override func update(_ currentTime: TimeInterval) {
        cameraNode.position.y = hero.position.y
        moveJoystickHiddenArea.position.y = hero.position.y
    }

    func setupHero(){
        //mi carica l'eroe, capire bene l'anchor point
        anchorPoint = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        hero.physicsBody = SKPhysicsBody(circleOfRadius: heroRadius)// il corpo fisico è dato uguale a quello definito prima
        hero.physicsBody?.affectedByGravity = false
        addChild(hero)
    }

    func setupBackground(){
        //mi carica il background, ho cambiato x ed y da 0.5 a 0
        addChild(background)
    }

    func setupCameraNode(){
        addChild(cameraNode)
    }

    func manageJoystick(){
        moveJoystickHiddenArea.joystick = analogJoystick
        analogJoystick.isMoveable = true

        addChild(moveJoystickHiddenArea)

        joystickActionSettings()
    }

    private func joystickActionSettings(){
        analogJoystick.on(.begin) { (joystick) in
            self.isCharacterMoving = true
        }
        analogJoystick.on(.move) { [unowned self] joystick in

            if self.isCharacterMoving == true{
                self.hero.position = CGPoint(x: self.hero.position.x + (joystick.velocity.x * self.velocityMultiplier), y: self.hero.position.y + (joystick.velocity.y * self.velocityMultiplier))
                    self.hero.zRotation = joystick.angular
            }
        }
        analogJoystick.on(.end) { (joystick) in
            self.hero.removeAllActions()
        }
    }
}
